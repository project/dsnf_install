
DSNF Install Profile 6.x-1.x, xxxx-xx-xx
---------------------------------------
by JamesAn: Added additional comments into code.
  Released first iteration of optional module installation, using Batch API to 
    install modules.

DSNF Install Profile 6.x-1.0-alpha, 2009-08-24
---------------------------------------
by JamesAn: Released first functional version.
  Finalized code structure for module selection task.
  Added capability to detect modules and module dependencies.
  Used Batch API to install modules
  Added scaffolding for optional module selection task.
by JamesAn: Expanded scaffolding with UI for the module selection task.
  Added automatic module detection in module selection task.
  Added links to project pages for modules not installed.
  Used External Links in module selection task to handle project page links.
  Added an empty second install task.
  Abstracted scaffolding to generate code to connect each install task to its 
    corresponding Form API definition and submission handler.
  Abstracted components of the DSNF in module selection screen so a list of 
    candidate modules is translated into a set of radio buttons filtered 
    through the module detector.
Fixed #501026 by JamesAn: changed order of enabling core modules.
by JamesAn: Initial scaffolding code.