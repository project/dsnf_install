<?php

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile,
 *   and optional 'language' to override the language selection for
 *   language-specific profiles, e.g., 'language' => 'fr'.
 */
function dsnf_install_profile_details() {
  return array(
    'name' => 'Drupal Social Network Framework',
    'description' => 'Select this profile to quickly set up social network features for Drupal',
  );
}

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to be enabled.
 */
function dsnf_install_profile_modules() {
  $core = array(
    'color', 'comment', 'dblog', 'help', 'locale', 'menu', 'path', 'poll',
    'profile', 'search', 'statistics', 'syslog', 'taxonomy', 'tracker', 'update', 'upload',
    // These have module dependencies.
    'forum', 'translation',
  );
  $contrib = array('devel', 'extlink');
  $custom = array();
  return array_merge($core, $contrib, $custom);
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during the final stage.
 *   The keys of the array will be used internally, while the values will be
 *   displayed to the user in the installer task list.
 */
function dsnf_install_profile_task_list() {
  return array(
    'module_selection' => st('Select SN modules'),
    'module_selection_batch' => st('Install SN modules'),
    'module_selection_opt' => st('Select opt. modules'),
    'module_selection_opt_batch' => st('Install opt. modules'),
  );
}

/**
 * Perform installation tasks for this installation profile.
 *
 * @param $task
 *   The task the installer is currently on.
 * @param $url
 *   The complete URL to be used in the link or form action to proceed with 
 *   the installation.
 */
function dsnf_install_profile_tasks(&$task, $url) {
  // Get array of installation tasks.
  $tasks = dsnf_install_profile_task_list();
  $task_keys = array_keys($tasks);
  $task_num = count($task_keys);

  if($task == 'profile') {
    // Set up all the things a default installation profile has.
    drupal_load('profile', 'default');
    default_profile_tasks($task, $url);

    drupal_rebuild_theme_registry();
    // Get current list of modules.
    $files = module_rebuild_cache();
    if($files['extlink']->status == 1) {
      variable_set('extlink_target', 1);
    }

    $task = $task_keys[0];
  }

  // Render batch pages during batch processes.
  if ($task == 'module_selection_batch') {
    include_once 'includes/batch.inc';
    return _batch_page();
  }
  elseif ($task == 'module_selection_opt_batch') {
    include_once 'includes/batch.inc';
    return _batch_page();
  }
  // Otherwise render the form for the current install task.
  else {
    // Flip task array so that the items are $task_key => $count to get the
    // current task number.
    $task_counts = array_flip($task_keys);
    $task_count = $task_counts[$task];

    // Render the current task's form.
    $output = drupal_get_form('dsnf_install_form_' . $task, $url);

    // If the form hasn't been processed yet, display it.
    if(!variable_get('dsnf_install_form_' . $task . '_submit', FALSE)) {
      drupal_set_title($tasks[$task]);
      return $output;
    }
    // Otherwise, go to the next task, if there are still tasks left.
    else if($task_count + 1 != $task_num) {
      $task = $task_keys[$task_count + 1];
    }
    // Otherwise, signal that the install profile is finished.
    else {
      // Return control to the installer.
      $task = 'profile-finished';
    }
  }

  return '';
}

/**
 * Form API array definition for the module_selection task.
 */
function dsnf_install_form_module_selection(&$form_state, $url) {
  // Include system admin CSS as its styles are used by the form's theme function.
  drupal_add_css(drupal_get_path('module', 'system') .'/admin.css', 'module');

  $sys_mod = system_modules();
  $dsnf_modules = _dsnf_install_modules('main', 'module');

  // Create modified system_modules form.
  $form = array(
    'validation_modules' => array(
      '#type' => 'value',
      '#value' => array_intersect_key($sys_mod['validation_modules']['#value'], $dsnf_modules),
    ),
    'disabled_modules' => array(
      '#type' => 'value',
      '#value' => array_intersect_key($sys_mod['disabled_modules']['#value'], $dsnf_modules),
    ),
    'name'        => array_intersect_key($sys_mod['name'], $dsnf_modules),
    'version'     => array_intersect_key($sys_mod['version'], $dsnf_modules),
    'description' => array_intersect_key($sys_mod['description'], $dsnf_modules),
    'status'      => $sys_mod['status'],
    'buttons'     => array(
      'submit'   => array(
        '#type'  => 'submit',
        '#value' => st('Save and continue'),
      ),
      'redetect' => array(
        '#type'  => 'button',
        '#value' => st('Redetect modules'),
      ),
    ),
    'url' => array(
      '#type' => 'value',
      '#value' => $url,
    ),
    '#theme'      => 'system_modules',
    '#submit'     => array('dsnf_install_form_module_selection_submit'),
    '#action'     => $url,
    '#redirect'   => FALSE,
  );
  $form['status']['#default_value']     = array_intersect_key($form['status']['#default_value'], $dsnf_modules);
  $form['status']['#options']           = array_intersect_key($form['status']['#options'], $dsnf_modules);
  $form['status']['#disabled_modules']  = array_intersect($form['status']['#disabled_modules'], array_keys($dsnf_modules));
  $form['status']['#incompatible_modules_php']  = array_intersect_key($form['status']['#incompatible_modules_php'], $dsnf_modules);
  $form['status']['#incompatible_modules_core'] = array_intersect_key($form['status']['#incompatible_modules_core'], $dsnf_modules);

  foreach (array_diff_key($dsnf_modules, $sys_mod['name']) as $module => $package) {
    $form['validation_modules']['#value'][$module] = new stdClass();
    $form['validation_modules']['#value'][$module]->name = $module;
    $form['disabled_modules']['#value'][$module] = FALSE;
    $form['name'][$module] = array('#value' => l($module, 'http://drupal.org/project/' . $module));
    $form['version'][$module] = array('#value' => '<span class="admin-missing">' . st('Missing') . '</span>');
    $form['description'][$module] = array('#value' => st("Not installed yet. Click on name to go to the module's page on Drupal.org."));
    $form['status']['#options'][$module] = '';
    $form['status']['#disabled_modules'][] = $module;
  }
  
  foreach ($dsnf_modules as $module => $package) {
    $form['validation_modules']['#value'][$module]->info['package'] = $package;
  }

  return $form;
}

/**
 * Form API validation for the module_selection task.
 */
function dsnf_install_form_module_selection_validate($form, &$form_state) {
  // Block form submission if the submit button is not pressed.
  if(!$form_state['submitted']) {
    return false;
  }

  // Ignore statuses of disabled_modules.
  $status = array_diff_key($form_state['values']['status'], $form_state['values']['disabled_modules']);

  // Remove all unset modules.
  $status = array_flip($status);
  if (isset($status[0])) {
    unset($status[0]);
  }

  // Ensure only one module, at most, is selected from each package.
  $error = FALSE;
  $dsnf_modules = _dsnf_install_modules('main', 'package');
  foreach ($dsnf_modules as $package => $modules) {
    if (count(array_intersect($modules, $status)) > 1) {
      form_set_error($package, t('Select only one @package module.', array('@package' => $package)));
      $error = TRUE;
    }
  }
  if ($error) {
    form_set_error('only-one', t('Only one module from each package can be selected. '));
    return false;
  }
}

/**
 * Form API submit for the module_selection task.
 */
function dsnf_install_form_module_selection_submit($form, &$form_state) {
  $module_list = module_rebuild_cache();
  // Reverse dependency list so root dependencies come first.
  $dependencies_groups = _dsnf_install_recurse_module_build_dependencies($module_list, $form_state['values']['status']);
  $dependencies = array();
  foreach ($dependencies_groups as $dependencies_group) {
    $dependencies = array_merge($dependencies, $dependencies_group);
  }

  $modules = $form_state['values']['status'];

  // Signal a successful submission.
  variable_set(__FUNCTION__, TRUE);

  $operations = array();
  foreach ($dependencies as $module) {
    $operations[] = array('_install_module_batch', array($module, $module_list[$module]->info['name']));
  }
  foreach ($modules as $module) {
    $operations[] = array('_install_module_batch', array($module, $module_list[$module]->info['name']));
  }
  $batch = array(
    'operations' => $operations,
    'finished' => '_dsnf_install_modules_batch_finished',
    'title' => st('Installing @drupal', array('@drupal' => drupal_install_profile_name())),
    'error_message' => st('The installation has encountered an error.'),
  );
  // Start a batch, switch to 'profile-install_batch' task. We need to
  // set the variable here, because batch_process() redirects.
  variable_set('install_task', 'module_selection_batch');
  batch_set($batch);
  batch_process($form_state['values']['url'], $form_state['values']['url']);
}

/**
 * Form API array definition for the module_selection_opt task.
 */
function dsnf_install_form_module_selection_opt(&$form_state, $url) {
  // Include system admin CSS as its styles are used by the form's theme function.
  drupal_add_css(drupal_get_path('module', 'system') .'/admin.css', 'module');

  $sys_mod = system_modules();
  $dsnf_modules = _dsnf_install_modules('optional', 'module');

  // Create modified system_modules form.
  $form = array(
    'validation_modules' => array(
      '#type' => 'value',
      '#value' => array_intersect_key($sys_mod['validation_modules']['#value'], $dsnf_modules),
    ),
    'disabled_modules' => array(
      '#type' => 'value',
      '#value' => array_intersect_key($sys_mod['disabled_modules']['#value'], $dsnf_modules),
    ),
    'name'        => array_intersect_key($sys_mod['name'], $dsnf_modules),
    'version'     => array_intersect_key($sys_mod['version'], $dsnf_modules),
    'description' => array_intersect_key($sys_mod['description'], $dsnf_modules),
    'status'      => $sys_mod['status'],
    'buttons'     => array(
      'submit'   => array(
        '#type'  => 'submit',
        '#value' => st('Save and continue'),
      ),
      'redetect' => array(
        '#type'  => 'button',
        '#value' => st('Redetect modules'),
      ),
    ),
    'url' => array(
      '#type' => 'value',
      '#value' => $url,
    ),
    '#theme'      => 'system_modules',
    '#submit'     => array('dsnf_install_form_module_selection_opt_submit'),
    '#action'     => $url,
    '#redirect'   => FALSE,
  );
  $form['status']['#default_value']     = array_intersect_key($form['status']['#default_value'], $dsnf_modules);
  $form['status']['#options']           = array_intersect_key($form['status']['#options'], $dsnf_modules);
  $form['status']['#disabled_modules']  = array_intersect($form['status']['#disabled_modules'], array_keys($dsnf_modules));
  $form['status']['#incompatible_modules_php']  = array_intersect_key($form['status']['#incompatible_modules_php'], $dsnf_modules);
  $form['status']['#incompatible_modules_core'] = array_intersect_key($form['status']['#incompatible_modules_core'], $dsnf_modules);

  foreach (array_diff_key($dsnf_modules, $sys_mod['name']) as $module => $package) {
    $form['validation_modules']['#value'][$module] = new stdClass();
    $form['validation_modules']['#value'][$module]->name = $module;
    $form['disabled_modules']['#value'][$module] = FALSE;
    $form['name'][$module] = array('#value' => l($module, 'http://drupal.org/project/' . $module));
    $form['version'][$module] = array('#value' => '<span class="admin-missing">' . st('Missing') . '</span>');
    $form['description'][$module] = array('#value' => st("Not installed yet. Click on name to go to the module's page on Drupal.org."));
    $form['status']['#options'][$module] = '';
    $form['status']['#disabled_modules'][] = $module;
  }
  
  foreach ($dsnf_modules as $module => $package) {
    $form['validation_modules']['#value'][$module]->info['package'] = $package;
  }

  return $form;
}

/**
 * Form API validation for the module_selection_opt task.
 */
function dsnf_install_form_module_selection_opt_validate($form, &$form_state) {
  // Block form submission if the submit button is not pressed.
  if(!$form_state['submitted']) {
    return false;
  }
}

/**
 * Form API submit for the module_selection_opt task.
 */
function dsnf_install_form_module_selection_opt_submit($form, &$form_state) {
  $module_list = module_rebuild_cache();
  // Reverse dependency list so root dependencies come first.
  $dependencies_groups = _dsnf_install_recurse_module_build_dependencies($module_list, $form_state['values']['status']);
  $dependencies = array();
  foreach ($dependencies_groups as $dependencies_group) {
    $dependencies = array_merge($dependencies, $dependencies_group);
  }

  $modules = $form_state['values']['status'];

  // Signal a successful submission.
  variable_set(__FUNCTION__, TRUE);

  $operations = array();
  foreach ($dependencies as $module) {
    $operations[] = array('_install_module_batch', array($module, $module_list[$module]->info['name']));
  }
  foreach ($modules as $module) {
    $operations[] = array('_install_module_batch', array($module, $module_list[$module]->info['name']));
  }
  $batch = array(
    'operations' => $operations,
    'finished' => '_dsnf_install_modules_opt_batch_finished',
    'title' => st('Installing @drupal', array('@drupal' => drupal_install_profile_name())),
    'error_message' => st('The installation has encountered an error.'),
  );
  // Start a batch, switch to 'profile-install_batch' task. We need to
  // set the variable here, because batch_process() redirects.
  variable_set('install_task', 'module_selection_opt_batch');
  batch_set($batch);
  batch_process($form_state['values']['url'], $form_state['values']['url']);
}

/**
 * Form API array definition for the confirmation task.
 */
function dsnf_install_form_confirmation(&$form_state, $url) {
  // Assemble entire form array.
  $form = array(
    '#action' => $url,
    '#redirect' => FALSE,
    'text' => array(
      '#value' => '<p>' . st('Click save and continue to finish the installation!') . '</p>',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => st('Save and continue'),
    ),
  );

  return $form;
}

/**
 * Form API submit for the confirmation task.
 */
function dsnf_install_form_confirmation_submit($form, &$form_state) {
  // Signal a successful submission.
  variable_set(__FUNCTION__, TRUE);
}

/*
 * Return list of SN modules that can be installed by this profile.
 *
 * @param $type
 *   Which list of modules to return. Possible values:
 *   - "main": Return the list of main SN modules.
 *   - "optional": Return the list of optional/helper modules.
 * @param $key
 *   Whether the array of modules is keyed by module name or by package.
 *   Possible values:
 *   - "package": The returned array is keyed by package names.
 *   - "module": The returned array is keyed by module machine names.
 * @return
 *   A keyed array of modules.
 */
function _dsnf_install_modules($type, $key) {
  if ($type == 'main') {
    $modules = array(
      'Forum' => array(
        'advanced_forum',
        'forumthread',
        'og_forum',
      ),
      'Private messaging' => array(
        'pm_lite',
        'privatemsg',
      ),
      'Contact list' => array(
        'user_relationships',
        'flag_friend',
      ),
      'Chatroom' => array(
        'chatroom',
        'phpfreechat',
      ),
      'Moderation' => array(
        'flag',
        'modr8',
      ),
    );
  }
  elseif ($type == 'optional') {
    $modules = array(
      'Admin' => array(
        'admin_menu',
        'devel',
      ),
    );
  }
  else {
    return array();
  }

  if ($key === 'package') {
    return $modules;
  }
  elseif ($key === 'module') {
    $all = array();
    foreach ($modules as $package => $module_package) {
      $module_keys = array();
      foreach ($module_package as $module) {
        $module_keys[$module] = $package;
      }
      $all = array_merge($all, $module_keys);
    }
    return $all;
  }
}

/*
 * Finished callback for the SN modules install batch.
 *
 * Advance installer task as the batch API doesn't do this for us.
 */
function _dsnf_install_modules_batch_finished($success, $results) {
  variable_set('install_task', 'module_selection_opt');
}

/*
 * Finished callback for the optional modules install batch.
 *
 * Advance installer task as the batch API doesn't do this for us.
 */
function _dsnf_install_modules_opt_batch_finished($success, $results) {
  variable_set('install_task', 'profile-finished');
  _dsnf_install_profile_cleanup();
}

/*
 * Do cleanup to complete installation and hand control back to Drupal.
 */
function _dsnf_install_profile_cleanup() {
  // Clear all installation variables.
  foreach($task_keys as $var) {
    variable_del('dsnf_install_form_' . $var . '_submit');
  }
}

/*
 * Recursively build dependency list for modules.
 * 
 * @param $module_list
 *   Array of available modules objects as outputted by module_rebuild_cache().
 * @param $modules
 *   Array of modules to be enabled keyed by their machine names.
 * @return
 *   An array of dependencies.
 */
function _dsnf_install_recurse_module_build_dependencies($module_list, $modules) {
  $dependencies = _dsnf_install_module_build_dependencies($module_list, $modules);
  if (!empty($dependencies)) {
    foreach ($dependencies as $dependency) {
      $dependencies = array_merge($dependencies, _dsnf_install_recurse_module_build_dependencies($module_list, $dependency));
    }
  }
  return $dependencies;
}

/*
 * Generate a list of dependencies for modules that are going to be enabled.
 *
 * Based on system_module_build_dependencies().
 * 
 * @param $module_list
 *   Array of available modules objects as outputted by module_rebuild_cache().
 * @param $modules
 *   Array of modules to be enabled keyed by their machine names.
 * @return
 *   An array of dependencies.
 */
function _dsnf_install_module_build_dependencies($module_list, $modules) {
  $dependencies = array();
  foreach ($module_list as $name => $module) {
    // If the module is disabled, will be switched on and it has dependencies.
    if (!$module->status && $modules[$name] && isset($module->info['dependencies'])) {
      foreach ($module->info['dependencies'] as $dependency) {
        if (isset($module_list[$dependency])) {
          if ($module_list[$dependency]->status != 1) {
            if (!isset($dependencies[$name])) {
              $dependencies[$name] = array();
            }
            $dependencies[$name][] = $dependency;
          }
        }
      }
    }
  }
  return $dependencies;
}